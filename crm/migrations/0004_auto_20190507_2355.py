# Generated by Django 2.1.7 on 2019-05-07 23:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0003_auto_20190507_2353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='identification_type',
            field=models.CharField(choices=[('RUT', 'DNI')], default='RUT', max_length=3),
        ),
    ]
