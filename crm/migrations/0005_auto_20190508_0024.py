# Generated by Django 2.1.7 on 2019-05-08 00:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0004_auto_20190507_2355'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='identification_type',
            field=models.CharField(choices=[('RUT', 'DNI')], default='RUT', max_length=2),
        ),
    ]
