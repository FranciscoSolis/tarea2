from django.contrib import admin
from crm.models import Client, GroupClient, ProfitCenter, Area, TypeService, Service, ClientService
#Robert Arevalo - Francisco Solis
# Register your models here.

class ClientAdmin(admin.ModelAdmin):
	list_display = ('name','identification_number','identification_type','business_name','address_1','group_client')
	list_filter = ('name','identification_number', 'identification_type', 'group_client')
	search_fields = ('name', 'identification_number')
admin.site.register(Client, ClientAdmin)

class GroupClientAdmin(admin.ModelAdmin):
	list_display = ('name','created_at')
	list_filter = ('name','created_at')
admin.site.register(GroupClient, GroupClientAdmin)

class ProfitCenterAdmin(admin.ModelAdmin):
	list_display = ('name','created_at')
	list_filter = ('name','created_at')
admin.site.register(ProfitCenter, ProfitCenterAdmin)

class AreaAdmin(admin.ModelAdmin):
	list_display = ('name','created_at')
	list_filter = ('name','created_at')
admin.site.register(Area, AreaAdmin)

class TypeServiceAdmin(admin.ModelAdmin):
	list_display = ('name','created_at')
	list_filter = ('name','created_at')
admin.site.register(TypeService, TypeServiceAdmin)

class ServiceAdmin(admin.ModelAdmin):
	list_display = ('name','type_service')
	list_filter = ('name','type_service')
	search_fields = ('name', 'type_service')
admin.site.register(Service, ServiceAdmin)

class ClientServiceAdmin(admin.ModelAdmin):
	list_display = ('amount','pay_day', 'type_sla','type_contract','client','service')
	list_filter = ('type_sla','type_contract','client','service')
admin.site.register(ClientService, ClientServiceAdmin)