from django.db import models
#Robert Arevalo - Francisco Solis
# Create your models here.

class GroupClient(models.Model):
	name = models.CharField(max_length=144)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class Client(models.Model):
	name = models.CharField(max_length=144)
	business_name = models.CharField(max_length=144)
	address_1 = models.CharField(max_length=144)
	identification_type_choice = (
		('DNI','DNI'),
		('RUT','RUT'),
		)
	identification_type = models.CharField(
		max_length=3, 
		choices=identification_type_choice, 
		default='RUT',
		)
	identification_number = models.CharField(max_length=50)
	tecnical_contact = models.EmailField(max_length=254, null=True, blank=True)	
	comercial_contact = models.EmailField(max_length=254, null=True, blank=True)	
	finance_contact = models.EmailField(max_length=254, null=True, blank=True)	
	group_client = models.ForeignKey(GroupClient, null=False, blank=False, on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class ProfitCenter(models.Model):
	name = models.CharField(max_length=144)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class Area(models.Model):
	name = models.CharField(max_length=144)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class TypeService(models.Model):
	name = models.CharField(max_length=144)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class Service(models.Model):
	name = models.CharField(max_length=144)
	comment = models.TextField()
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)
	area = models.ForeignKey(Area, null=False, blank=False, on_delete=models.CASCADE)
	profit_center = models.ForeignKey(ProfitCenter, null=False, blank=False, on_delete=models.CASCADE)
	type_service= models.ForeignKey(TypeService, null=False, blank=False, on_delete=models.CASCADE)

	def __str__(self):
		return self.name

class ClientService(models.Model):
	amount = models.DecimalField(max_digits=15, decimal_places=2)
	pay_day = models.IntegerField()
	type_sla_choice = (
		('8x5','8x5'),('24x7','24x7'),
		)
	type_sla = models.CharField(
		max_length=10,
		choices=type_sla_choice,
		default='8x5',
		)
	type_contract_choice = (
		('definido','definido'),('indefinido','indefinido'),
		)
	type_contract = models.CharField(
		max_length=10,
		choices=type_contract_choice,
		default='definido',
		)
	start_date = models.DateTimeField(auto_now_add=True)
	end_date = models.DateTimeField(auto_now_add=True)
	service = models.ForeignKey(Service, null=False, blank=False, on_delete=models.CASCADE)
	client = models.ForeignKey(Client, null=False, blank=False, on_delete=models.CASCADE)
	profit_center = models.ForeignKey(ProfitCenter, null=False, blank=False, on_delete=models.CASCADE)

